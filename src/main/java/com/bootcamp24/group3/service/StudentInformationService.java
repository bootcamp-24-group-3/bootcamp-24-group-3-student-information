package com.bootcamp24.group3.service;

import com.bootcamp24.group3.domain.StudentInformation;
import com.bootcamp24.group3.repository.StudentInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentInformationService {

    @Autowired
    private StudentInformationRepository studentInformationRepository;

    public List<StudentInformation> informationList() {
        return studentInformationRepository.findAll();
    }

    public void save(StudentInformation studentInformation) {
        studentInformationRepository.save(studentInformation);
    }

    public StudentInformation get(Integer id) {
        return studentInformationRepository.findById(id).get();
    }

    public void deleteStudent(Integer id) {
        studentInformationRepository.deleteById(id);
    }

}
