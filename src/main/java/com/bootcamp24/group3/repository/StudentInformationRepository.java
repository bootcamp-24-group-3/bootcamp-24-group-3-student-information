package com.bootcamp24.group3.repository;

import com.bootcamp24.group3.domain.StudentInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentInformationRepository extends JpaRepository<StudentInformation, Integer> {

}
