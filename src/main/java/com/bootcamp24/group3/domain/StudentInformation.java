package com.bootcamp24.group3.domain;

import javax.persistence.*;

@Entity
@Table(name = "student_information_table")
public class StudentInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int IDNumber;

    @Column(name = "Full_Name")
    private String name;

    @Column(name = "Age")
    private int age;

    @Column(name = "Address")
    private String address;

    @Column(name = "Region")
    private String region;

    @Column(name = "Zip_Code")
    private int zipCode;

    @Column(name = "City")
    private String city;

    @Column(name = "Province")
    private String province;

    @Column(name = "Phone_Number")
    private int phoneNumber;

    @Column(name = "Mobile_Number")
    private int mobileNumber;

    @Column(name = "Email")
    private String email;

    @Column(name = "Year_Level")
    private int yearLevel;

    @Column(name = "Section")
    private String section;

    public StudentInformation() {
        super();
    }

    public StudentInformation(int IDNumber, String name, int age, String address, String region, int zipCode, String city, String province, int phoneNumber, int mobileNumber, String email, int yearLevel, String section) {
        super();
        this.IDNumber = IDNumber;
        this.name = name;
        this.age = age;
        this.address = address;
        this.region = region;
        this.zipCode = zipCode;
        this.city = city;
        this.province = province;
        this.phoneNumber = phoneNumber;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.yearLevel = yearLevel;
        this.section = section;
    }

    public String getAddress() {
        return address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getIDNumber() {
        return IDNumber;
    }

    public void setIDNumber(int IDNumber) {
        this.IDNumber = IDNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(int mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getYearLevel() {
        return yearLevel;
    }

    public void setYearLevel(int yearLevel) {
        this.yearLevel = yearLevel;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
