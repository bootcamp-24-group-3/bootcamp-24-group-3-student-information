package com.bootcamp24.group3.controller;

import com.bootcamp24.group3.domain.StudentInformation;
import com.bootcamp24.group3.service.StudentInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class StudentController {

    @Autowired
    private StudentInformationService studentInformationService;

    @GetMapping("/")
    public String viewHomePage(Model model) {
        List<StudentInformation> studentList = studentInformationService.informationList();
        model.addAttribute("studentList", studentList);
        System.out.println("Get / ");
        return "index.html";
    }

    @GetMapping("/new")
    public String add(Model model) {
        model.addAttribute("student", new StudentInformation());
        return "new.html";
    }

    @PostMapping(value = "/save")
    public String saveStudent(@ModelAttribute("student") StudentInformation student) {
        studentInformationService.save(student);
        return "redirect:/";
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView showEditStudentPage(@PathVariable(name = "id") int id) {
        ModelAndView modelAndView = new ModelAndView("new");
        StudentInformation std = studentInformationService.get(id);
        modelAndView.addObject("student", std);
        return modelAndView;
    }

    @RequestMapping("/delete/{id}")
    public String deleteStudent(@PathVariable(name = "id") int id) {
        studentInformationService.deleteStudent(id);
        return "redirect:/";
    }

}
